import api from '@/store/api.js'

const state = {
  username: ''
}

const mutations = {
  setUsername: function (state, username) {
    state.username = username
  }
}

const getters = {
  isAuthentificated: state => {
    return state.username.length > 0
  }
}

const actions = {
  getUser ({ commit }) {
    return api.getUser().then(success => {
      commit('setUsername', success.data.name)
    })
  },
  logout ({ commit }) {
    commit('setUsername', '')
  }
}

export default {
  state: () => state,
  mutations: mutations,
  getters: getters,
  actions: actions
}
