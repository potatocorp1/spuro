import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/user.js'
import token from '@/store/token.js'
import api from '@/store/api.js'

Vue.use(Vuex)

const state = {
  activities: [],
  logs: [],
  tags: [],
  loadingStatus: 'NOT_YET'
}

export const mutations = {
  SET_ACTIVITIES: (state, activities) => {
    state.activities = activities
  },
  ADD_ACTIVITY: (state, activity) => {
    state.activities.push(activity)
  },
  UPDATE_ACTIVITY: (state, { id, name, tags, choices }) => {
    const activityIndex = state.activities.findIndex(activity => activity.id === id)
    const activity = state.activities[activityIndex]
    activity.name = name
    activity.tags = tags ?? []
    if (choices !== undefined) {
      activity.choices = choices
    }
    state.activities.splice(activityIndex, 1, activity)
  },
  DELETE_ACTIVITY: (state, id) => {
    const activityIndex = state.activities.findIndex(activity => activity.id === id)
    state.activities.splice(activityIndex, 1)
  },
  SET_STATUS: (state, status) => {
    state.loadingStatus = status
  },

  SET_LOGS: (state, logs) => {
    state.logs = state.logs.concat(logs)
  },
  INIT_LOGS_STATUS: (state, id) => {
    const activityIndex = state.activities.findIndex(activity => activity.id === id)
    const activity = state.activities[activityIndex]
    activity.initLogs = true
    state.activities.splice(activityIndex, 1, activity)
  },
  ADD_LOG: (state, log) => {
    state.logs.push(log)
  },
  UPDATE_LOG: (state, { id, date, value }) => {
    const logId = state.logs.findIndex(activity => activity.id === id)
    const log = state.logs[logId]
    if (date !== undefined) {
      log.date = date
    }
    if (value !== undefined) {
      log.value = value
    }
    state.logs.splice(logId, 1, log)
  },
  DELETE_LOG: (state, id) => {
    const logId = state.logs.findIndex(log => log.id === id)
    state.logs.splice(logId, 1)
  },

  SET_TAGS: (state, tags) => {
    state.tags = tags
  },
  ADD_TAG: (state, tag) => {
    state.tags.push(tag)
  },
  UPDATE_TAG: (state, { id, name }) => {
    const tagIndex = state.tags.findIndex(tag => tag.id === id)
    const tag = state.tags[tagIndex]
    tag.name = name
    state.tags.splice(tagIndex, 1, tag)
  },
  DELETE_TAG: (state, id) => {
    const tagIndex = state.tags.findIndex(tag => tag.id === id)
    state.tags.splice(tagIndex, 1)
  }
}

export const getters = {
  activity: state => (id) => {
    return state.activities.find(activity => activity.id === id)
  },
  choiceName: state => ({ activityId, choiceId }) => {
    const activity = state.activities.find(activity => activity.id === activityId)
    const choice = activity.choices.find(choice => choice.id === choiceId)
    return choice.name
  },
  logs: state => (activityId) => {
    return state.logs.filter(log => log.activity_id === activityId)
  },
  tags: state => state.tags,
  tagIsCheckOnActivity: state => ({ tagId, activityId }) => {
    const activity = state.activities.find(activity => activity.id === activityId)
    return activity.tags.find(tag => tag === tagId) !== undefined
  },
  status: state => state.loadingStatus,
  lastLogDate: state => (activityId) => {
    const logs = state.logs.filter(log => log.activity_id === activityId)
    const logsDates = logs.map(log => log.date)
    const lastLogDate = logsDates.sort().pop()
    return lastLogDate
  }
}

export const actions = {
  getActivities: ({ commit, state }) => {
    if (state.loadingStatus === 'NOT_YET') {
      api.getActivities()
        .then(response => {
          const activities = []
          response.data.data.forEach((activity) => {
            let tags = []
            if (activity.tags) {
              tags = activity.tags.map(tag => tag.id)
            }
            activities.push({
              id: activity.id ?? null,
              name: activity.name ?? null,
              unit: activity.unit ?? null,
              tags: tags,
              initLogs: false
            })
          })
          commit('SET_ACTIVITIES', activities)
          commit('SET_STATUS', 'OK')
        })
        .catch(() => commit('SET_STATUS', 'KO'))
    }
  },
  addActivity: ({ commit }, activity) => {
    return new Promise((resolve, reject) => {
      api.addActivity(activity)
        .then(response => {
          activity.id = response.data.id
          activity.choices = []
          if (response.data.choices) {
            response.data.choices.forEach((choice) => {
              activity.choices.push({
                id: choice.id,
                name: choice.name
              })
            })
          }
          activity.initLogs = true
          commit('ADD_ACTIVITY', activity)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  updateActivity: ({ commit }, activity) => {
    return new Promise((resolve, reject) => {
      api.updateActivity(activity)
        .then(response => {
          commit('UPDATE_ACTIVITY', activity)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  deleteActivity: ({ commit }, id) => {
    return new Promise((resolve, reject) => {
      api.deleteActivity(id)
        .then(response => {
          commit('DELETE_ACTIVITY', id)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  initChoices: ({ commit, state }, activityId) => {
    const activity = state.activities.find(activity => activity.id === activityId)
    if (activity.unit === 'choice' && activity.choices === undefined) {
      api.getChoices(activityId)
        .then(response => {
          const choices = []
          response.data.data.forEach((choice) => {
            choices.push({
              id: choice.id ?? null,
              name: choice.name ?? null
            })
          })
          activity.choices = choices
          commit('UPDATE_ACTIVITY', activity)
        })
    }
  },
  addChoice: ({ commit, state }, { activityId, name }) => {
    return new Promise((resolve, reject) => {
      api.addChoice(activityId, { name: name })
        .then(response => {
          const newChoice = {
            id: response.data.id,
            name: response.data.name
          }
          const activity = state.activities.find(activity => activity.id === activityId)
          activity.choices.push(newChoice)
          commit('UPDATE_ACTIVITY', activity)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  updateChoice: ({ commit, state }, choice) => {
    return new Promise((resolve, reject) => {
      api.updateChoice(choice)
        .then(response => {
          const newChoice = response.data
          const activity = state.activities.find(activity => activity.id === newChoice.activity_id)
          const choices = activity.choices
          const choiceIndex = choices.findIndex(choice => choice.id === newChoice.id)
          const choiceToSave = choices[choiceIndex]
          choiceToSave.name = newChoice.name
          choices.splice(choiceIndex, 1, choiceToSave)
          activity.choices = choices
          commit('UPDATE_ACTIVITY', activity)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  deleteChoice: ({ commit, state }, { choiceId, activityId }) => {
    let logs = state.logs.filter(log => log.activity_id === activityId)
    logs = logs.filter(log => log.value === choiceId)
    if (logs.length === 0) {
      return new Promise((resolve, reject) => {
        api.deleteChoice(choiceId)
          .then(response => {
            const activity = state.activities.find(activity => activity.id === activityId)
            const choices = activity.choices
            const choiceIndex = choices.findIndex(choice => choice.id === choiceId)
            choices.splice(choiceIndex, 1)
            activity.choices = choices
            commit('UPDATE_ACTIVITY', activity)
            resolve(response)
          })
          .catch((error) => {
            reject(error.response.data.errors)
          })
      })
    }
  },
  initLogs: ({ commit, state }, activityId) => {
    const activity = state.activities.find(activity => activity.id === activityId)
    if (activity.initLogs === false) {
      api.getLogs(activityId)
        .then(response => {
          const logs = []
          response.data.data.forEach((log) => {
            logs.push({
              id: log.id,
              date: log.date,
              value: log.value,
              activity_id: log.activity_id
            })
          })
          commit('SET_LOGS', logs)
          commit('INIT_LOGS_STATUS', activityId)
        })
    }
  },
  addLog: ({ commit }, log) => {
    return new Promise((resolve, reject) => {
      api.addLog(log.activity_id, log)
        .then(response => {
          log.id = response.data.id
          commit('ADD_LOG', log)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  updateLog: ({ commit }, log) => {
    return new Promise((resolve, reject) => {
      api.updateLog(log)
        .then(response => {
          commit('UPDATE_LOG', log)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  deleteLog: ({ commit }, id) => {
    return new Promise((resolve, reject) => {
      api.deleteLog(id)
        .then(response => {
          commit('DELETE_LOG', id)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  getTags: ({ commit }) => {
    api.getTags()
      .then(response => {
        const tags = []
        response.data.data.forEach((tag) => {
          tags.push({
            id: tag.id ?? null,
            name: tag.name ?? null
          })
        })
        commit('SET_TAGS', tags)
      })
  },
  addTag: ({ commit }, tag) => {
    return new Promise((resolve, reject) => {
      api.addTag(tag)
        .then(response => {
          tag.id = response.data.id
          commit('ADD_TAG', tag)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  updateTag: ({ commit }, tag) => {
    return new Promise((resolve, reject) => {
      api.updateTag(tag)
        .then(response => {
          commit('UPDATE_TAG', tag)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  },
  deleteTag: ({ commit }, id) => {
    return new Promise((resolve, reject) => {
      api.deleteTag(id)
        .then(response => {
          commit('DELETE_TAG', id)
          resolve(response)
        })
        .catch((error) => {
          reject(error.response.data.errors)
        })
    })
  }
}

export default new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions,
  strict: true,
  modules: {
    user,
    token
  }
})
