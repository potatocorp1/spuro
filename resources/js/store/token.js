import api from '@/store/api.js'

const state = {
  tokens: [],
  scopes: []
}

const mutations = {
  setTokens: function (state, tokens) {
    state.tokens = tokens
  },
  revokeToken: function (state, tokenId) {
    const token = state.tokens.find(token => token.id === tokenId)
    const index = state.tokens.indexOf(token)
    token.revoked = true
    state.tokens.splice(index, 1, token)
  },
  setScopes: function (state, scopes) {
    state.scopes = scopes
  },
  addToken: function (state, token) {
    state.tokens.push(token)
  }
}

const actions = {
  getTokens ({ commit }) {
    return api.getTokens().then(success => {
      commit('setTokens', success.data)
    })
  },
  revokeToken ({ commit }, payload) {
    return api.revokeToken(payload).then(() => {
      commit('revokeToken', payload)
    })
  },
  getScopes ({ commit }) {
    return api.getScopes().then(success => {
      commit('setScopes', success.data)
    })
  },
  createToken ({ commit }, payload) {
    return api.addToken(payload.name, payload.scopeChecked).then(success => {
      commit('addToken', success.data.token)
      return success
    })
  }
}

export default {
  state: () => state,
  mutations: mutations,
  actions: actions
}
