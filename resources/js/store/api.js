import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

export default {
  register: function (formData) {
    return axios.post('/register', formData)
  },
  getUser: function () {
    return axios.get('/api/user')
  },
  getActivities: function () {
    return axios.get('/api/activities')
  },
  addActivity: function (activity) {
    return axios.post('/api/activities', activity)
  },
  updateActivity: function (activity) {
    return axios.put('/api/activities/' + activity.id, { name: activity.name, tags: activity.tags ?? [] })
  },
  deleteActivity: function (id) {
    return axios.delete('/api/activities/' + id)
  },
  getChoices: function (activityId) {
    return axios.get('/api/activities/' + activityId + '/choices')
  },
  addChoice: function (activityId, choice) {
    return axios.post('/api/activities/' + activityId + '/choices', choice)
  },
  updateChoice: function (choice) {
    return axios.put('/api/choices/' + choice.id, { name: choice.name })
  },
  deleteChoice: function (id) {
    return axios.delete('/api/choices/' + id)
  },
  getLogs: function (activityId) {
    return axios.get('/api/activities/' + activityId + '/logs')
  },
  addLog: function (activityId, log) {
    return axios.post('/api/activities/' + activityId + '/logs', log)
  },
  updateLog: function (log) {
    return axios.put('/api/logs/' + log.id, { date: log.date, value: log.value })
  },
  deleteLog: function (id) {
    return axios.delete('/api/logs/' + id)
  },
  getTags: function () {
    return axios.get('/api/tags/')
  },
  addTag: function (tag) {
    return axios.post('/api/tags', tag)
  },
  updateTag: function (tag) {
    return axios.put('/api/tags/' + tag.id, { name: tag.name })
  },
  deleteTag: function (id) {
    return axios.delete('/api/tags/' + id)
  },

  getScopes: function () {
    return axios.get('/oauth/scopes')
  },
  getTokens: function () {
    return axios.get('/oauth/personal-access-tokens')
  },
  addToken: function (name, scopes) {
    const data = {
      name: name,
      scopes: scopes
    }
    return axios.post('/oauth/personal-access-tokens', data)
  },
  revokeToken: function (id) {
    return axios.delete('/oauth/personal-access-tokens/' + id)
  }
}
