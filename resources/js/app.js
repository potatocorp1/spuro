/* eslint-env node */

require('./bootstrap');

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

Sentry.init({
  dsn: process.env.SENTRY_VUE_DSN,
  integrations: [new Integrations.Vue({ Vue, attachProps: true })]
})

Vue.config.productionTip = false

Vue.filter('displayDate', function (value) {
  if (!value) return 'Aucun log'
  return new Date(value).toLocaleString().split(/\D/).slice(0, 3).map(num => num.padStart(2, '0')).join('/')
})

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
