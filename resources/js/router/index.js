import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '../views/HomePage.vue'
import TheMenu from '../components/TheMenu.vue'

Vue.use(VueRouter)

Vue.component('the-menu', TheMenu)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/new-activity',
    name: 'new-activity',
    component: () => import('../views/ActivityCreation.vue')
  },
  {
    path: '/update-activity',
    props: route => ({ id: parseInt(route.query.id) }),
    component: () => import('../views/ActivityUpdate.vue'),
    name: 'update-activity'
  },
  {
    path: '/new-log',
    props: route => ({ activityId: parseInt(route.query.activityId) }),
    component: () => import('../views/LogForm.vue'),
    name: 'new-log'
  },
  {
    path: '/stats',
    component: () => import('../views/WorkInProgress.vue'),
    name: 'stats'
  },
  {
    path: '/profile',
    component: () => import('../views/ProfilePage.vue'),
    name: 'profile'
  },
  {
    path: '*',
    redirect: '/' // page 404
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
