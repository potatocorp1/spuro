@extends('layouts.app')

@section('content')
<div class="flex content-center h-full items-center justify-center">
    <div>
        <h1 class="text-4xl">Connexion</h1>

        <form method="POST" action="{{ route('login') }}" class="w-full py-8">
            @csrf

            <div class="pb-4">
                <label for="email">Adresse mail</label>
                <div>
                    <input id="email" type="email" class="border-2" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="pb-4">
                <label for="password" class="">Mot de passe</label>
                <div class="">
                    <input id="password" type="password" class="border-2" name="password" required autocomplete="current-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="pb-4">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">Se souvenir de moi</label>
            </div>

            <div class="flex items-center justify-center">
                <button type="submit" class="border-2 py-2 px-4 bg-gray-200">
                    Login
                </button>
            </div>
        </form>

        <div>
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}">Mot de passe oublié ?</a>
            @endif
        </div>
        <div>
            <a href="{{ route('register') }}">Créer un compte</a>
        </div>
    </div>
</div>
@endsection
