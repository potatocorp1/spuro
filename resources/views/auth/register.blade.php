@extends('layouts.app')

@section('content')
<div class="flex content-center h-full items-center justify-center">
    <div>
        <h1 class="text-4xl">{{ __('Créer un compte') }}</h1>

        <form method="POST" action="{{ route('register') }}" class="w-full py-8">
            @csrf

            <div class="pb-4">
                <label for="name">Nom</label>
                <div>
                    <input id="name" type="text" class="border-2" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                    @error('name')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="pb-4">
                <label for="email">Adresse mail</label>
                <div>
                    <input id="email" type="email" class="border-2" name="email" value="{{ old('email') }}" required autocomplete="email">
                    @error('email')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="pb-4">
                <label for="password">Mot de passe</label>
                <div>
                    <input id="password" type="password" class="border-2" name="password" required autocomplete="new-password">
                    @error('password')
                        <span role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="pb-4">
                <label for="password-confirm">Confirmer le mot de passe</label>
                <div>
                    <input id="password-confirm" type="password" class="form-control border-2" name="password_confirmation" required autocomplete="new-password">
                </div>
            </div>

            <div class="flex items-center justify-center">
                <button type="submit" class="border-2 py-2 px-4 bg-gray-200">Créer</button>
            </div>
        </form>

        <div>
            Déjà un compte ? <a href="{{ route('login') }}">Se connecter</a>
        </div>
    </div>
</div>
@endsection
