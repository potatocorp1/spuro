<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;

Route::get('/openAPI.yaml', function () {
    $path = base_path('doc/openAPI/spec.yaml');
    return response()->file($path);
});

Auth::routes();

Route::any('/doc/api', function () {
    return view('doc-api');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/', [HomeController::class, 'index']);

    Route::any('{all}', [HomeController::class, 'index'])
    ->where('all', '^(?!api).*$');
});
