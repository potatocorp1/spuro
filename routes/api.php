<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\ChoiceController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SharingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api', 'scope:all,read:activities'])->get('/activities', [ActivityController::class, 'index']);
Route::middleware(['auth:api', 'scope:all,write:activities'])->group(function () {
    Route::post('/activities', [ActivityController::class, 'store']);
    Route::put('/activities/{activity}', [ActivityController::class, 'update']);
    Route::delete('/activities/{activity}', [ActivityController::class, 'destroy']);
});

Route::middleware(['auth:api', 'scope:all,read:logs'])->get('/activities/{activity}/logs', [LogController::class, 'index']);
Route::middleware(['auth:api', 'scope:all,write:logs'])->group(function () {
    Route::post('/activities/{activity}/logs', [LogController::class, 'store']);
    Route::put('/logs/{log}', [LogController::class, 'update']);
    Route::delete('/logs/{log}', [LogController::class, 'destroy']);
});

Route::middleware(['auth:api', 'scope:all,read:activities'])->get('/activities/{activity}/choices', [ChoiceController::class, 'index']);
Route::middleware(['auth:api', 'scope:all,write:activities'])->post('/activities/{activity}/choices', [ChoiceController::class, 'store']);
Route::middleware(['auth:api', 'scope:all,write:activities'])->put('/choices/{choice}', [ChoiceController::class, 'update']);
Route::middleware(['auth:api', 'scope:all,write:activities'])->delete('/choices/{choice}', [ChoiceController::class, 'destroy']);

Route::middleware(['auth:api', 'scope:all,read:tags'])->get('/tags', [TagController::class, 'index']);
Route::middleware(['auth:api', 'scope:all,write:tags'])->post('/tags', [TagController::class, 'store']);
Route::middleware(['auth:api', 'scope:all,write:tags'])->put('/tags/{tag}', [TagController::class, 'update']);
Route::middleware(['auth:api', 'scope:all,write:tags'])->delete('/tags/{tag}', [TagController::class, 'destroy']);

Route::middleware(['auth:api', 'scope:all,read:sharings'])->get('/sharings', [SharingController::class, 'index']);
Route::middleware(['auth:api', 'scope:all,write:sharings'])->put('/tags/{tag}/sharing', [SharingController::class, 'store']);
Route::middleware(['auth:api', 'scope:all,write:sharings'])->delete('/tags/{tag}/sharing/{user}', [SharingController::class, 'destroy']);

Route::middleware(['auth:api', 'scope:all,read:users'])->get('/user', [UserController::class, 'current']);
