<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Activity;
use App\Models\User;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        if ($user === null) {
            $user = User::factory()->create();
        }

        Activity::factory()
            ->create(['user_id' => $user->id]);
    }
}
