<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        if ($user === null) {
            $user = User::factory()->create();
        }

        $activity = Activity::first();
        if ($activity === null) {
            $activity = Activity::factory()->create();
        }

        Log::factory()
            ->create([
                'user_id' => $user->id,
                'activity_id' => $activity->id,
            ]);
    }
}
