<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Activity;
use App\Models\User;
use App\Models\Choice;

class ChoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        if ($user === null) {
            $user = User::factory()->create();
        }

        $activity = Activity::factory()
            ->create([
                'user_id' => $user->id,
                'unit' => 'choice',
            ]);

        Choice::factory()->count(3)->create(['activity_id' => $activity->id]);
    }
}
