# Prérequis

- PHP >= 7.3
- sqlite 3 PHP extension

et [prérequis Laravel](https://laravel.com/docs/8.x/deployment#server-requirements)

## Xdebug

Pour avoir la couverture de test et les tests par mutant (infection), il faut activer xdebug.
 - installer xdebug pour PHP >= 7.3
 - Modifier la configuration dans /etc/php/mods/xdebug.ini et ajouter la ligne suivante
```ini
xdebug.mode=coverage
```

# Installation

 * composer install
 * touch database/database.sqlite
 * cp .env.example .env

modifier le chemin de la db dans .env à la clé DB_DATABASE

 * php artisan key:generate
 * php artisan migrate

# After a pull

 * composer install
 * npm install
 * php artisan migrate

# How to use API

Init first grant with

```bash
php artisan passport:install
```

The return must be keeped in your notes:
for exemple:
```
Encryption keys generated successfully.
Personal access client created successfully.
Client ID: 1
Client secret: hash
Password grant client created successfully.
Client ID: 2
Client secret: hash
```

# Serve

php artisan serve

et se rendre sur [http://localhost:8000](http://localhost:8000)

# Serve API documentation

npm run watch-doc-api
