<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ClearBusinessData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'business:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate activities, activity_tag, choices, logs, sharings and tags tables. Available only in dev';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (config('app.env') !== 'local') {
            $this->error('You must run this command in local env');
            return 1;
        }

        if ($this->confirm('Do you really wish to run this command?')) {
            Schema::disableForeignKeyConstraints();
            DB::table('activity_tag')->truncate();
            DB::table('activities')->truncate();
            DB::table('choices')->truncate();
            DB::table('logs')->truncate();
            DB::table('tags')->truncate();
            $this->info('Truncated');
        }

        return 0;
    }
}
