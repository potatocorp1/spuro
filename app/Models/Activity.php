<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'unit',
    ];

    /**
     * The attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'unit' => 'string',
        'user_id' => 'integer',
    ];

    /**
     * Get the user that owns this activity.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the logs for the activity.
     */
    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    /**
     * The tags that belong to the activity.
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Get the choices for the activity.
     */
    public function choices()
    {
        return $this->hasMany(Choice::class);
    }
}
