<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Choice extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'activity_id' => 'integer',
    ];

    /**
     * Get the activity that owns this choice.
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
}
