<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'value',
    ];

    /**
     * The attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime',
        'value' => 'integer',
        'user_id' => 'integer',
        'activity_id' => 'integer',
    ];

    /**
     * Get the user that owns this log.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the activity that owns this log.
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
}
