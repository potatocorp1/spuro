<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast to native types
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'user_id' => 'integer',
    ];

    protected $hidden = ['pivot'];

    /**
     * The activities that belong to the tag.
     */
    public function activities()
    {
        return $this->belongsToMany(Activity::class);
    }

    /**
     * Get the user that owns this activity.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the users for this shared tag.
     */
    public function sharing()
    {
        return $this->belongsToMany(User::class, 'sharing');
    }
}
