<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\Choice;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LogController extends Controller
{
    private array $rules = [
        'date' => 'required|date',
        'value' => 'required|integer',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function index(Activity $activity)
    {
        $logs = Log::where([
                ['user_id', Auth::user()->id],
                ['activity_id', $activity->id],
            ])
            ->paginate(15);
        return response()->json($logs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\Activity  $activity
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Activity $activity, Request $request)
    {
        if ($activity->unit == "choice") {
            $this->rules['value'] = [
                'required',
                Rule::exists('choices', 'id')->where(function ($query) use ($activity) {
                    return $query->where('activity_id', $activity->id);
                })
            ];
        }

        $validatedData = $request->validate($this->rules);

        $log = new Log($request->all());
        $log->user_id = Auth::user()->id;
        $log->activity_id = $activity->id;
        $log->save();

        return response()->json($log, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Log $log)
    {
        $activity = $log->activity;
        if ($activity->unit == "choice") {
            $this->rules['value'] = [
                'required',
                Rule::exists('choices', 'id')->where(function ($query) use ($activity) {
                    return $query->where('activity_id', $activity->id);
                })
            ];
        }

        $validatedData = $request->validate($this->rules);

        $log->fill($request->all());
        $log->save();

        return response()->json($log, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Log  $log
     * @return \Illuminate\Http\Response
     */
    public function destroy(Log $log)
    {
        $log->delete();
        return response(null, 204);
    }
}
