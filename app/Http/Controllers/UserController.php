<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display the current user
     *
     * @return \Illuminate\Http\Response
     */
    public function current()
    {
        return response()->json(Auth::user());
    }
}
