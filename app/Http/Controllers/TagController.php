<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::leftJoin('sharing', 'tags.id', '=', 'sharing.tag_id')
            ->where('tags.user_id', Auth::user()->id)
            ->orWhere('sharing.user_id', Auth::user()->id)
            ->orderBy('name')
            ->paginate(15);
        return response()->json($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255|unique:tags',
        ]);

        $limit = 20;
        $tags = Tag::where('user_id', Auth::user()->id)->count();
        if ($tags >= $limit) {
            return response()->json([
                'errors' => [
                    'tag' => 'Too many tags, limit is ' . $limit
                ]
            ], 422);
        }

        $tag = new Tag($request->all());
        $tag->user_id = Auth::user()->id;
        $tag->save();

        return response()->json($tag, 201);
    }

    /**
     * Update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        if ($tag->user_id != Auth::user()->id) {
            return response()->json([], 403);
        }

        $validatedData = $request->validate([
            'name' => 'required|max:255',
        ]);

        $tag->fill($validatedData);
        $tag->save();

        return response()->json($tag, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();
        return response(null, 204);
    }
}
