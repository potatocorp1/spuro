<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SharingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Auth::user()
            ->sharing()
            ->paginate(15);
        
        $tags->setCollection($tags->getCollection()->map(function ($tag) {
            return [
                'tag_id' => $tag->id,
                'guest_id' => $tag->user_id,
                'guest_name' => $tag->user->name,
            ];
        }));
        return response()->json($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag           $tag
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tag $tag)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $user = User::where('email', $request->email)->first();
        $tag->sharing()->attach($user);
        return response()->json([], 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag, User $user)
    {
        if (Auth::user()->id === $user->id || Auth::user()->id === $tag->user_id) {
            $tag->sharing()->detach($user);
            return response(null, 204);
        } else {
            return response(null, 403);
        }
    }
}
