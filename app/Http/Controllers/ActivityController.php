<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Choice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ActivityController extends Controller
{
    private array $rules = [
        'name' => 'required|max:255',
        'unit' => 'required|in:duration,numeric,choice',
        'choices' => 'required_if:unit,choice|array',
        'choices.*' => 'required',
        'tags' => 'nullable|array',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = Activity::where('user_id', Auth::user()->id)
            ->orderBy('name')
            ->with('tags:id,name,user_id')
            ->paginate(15);
        return response()->json($activities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $this->rules['tags.*'] = [
            'required',
            Rule::exists('tags', 'id')->where(function ($query) use ($userId) {
                return $query->where('user_id', $userId);
            }),
        ];

        $validatedData = $request->validate($this->rules);

        $activity = new Activity($request->all());
        $activity->user_id = Auth::user()->id;
        $activity->save();

        if ($validatedData['unit'] == 'choice') {
            foreach ($validatedData['choices'] as $choiceName) {
                $choice = new Choice([
                    'name' => $choiceName,
                ]);
                $activity->choices()->save($choice);
            }

            $activity->load('choices');
        }

        if (isset($validatedData['tags'])) {
            $activity->tags()->sync($validatedData['tags']);
            $activity->load('tags');
        }

        return response()->json($activity, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        $userId = Auth::user()->id;

        unset($this->rules['unit']);
        $this->rules['tags.*'] = [
            'required',
            Rule::exists('tags', 'id')->where(function ($query) use ($userId) {
                return $query->where('user_id', $userId);
            }),
        ];
        $validatedData = $request->validate($this->rules);

        $activity->fill($validatedData);
        if (isset($validatedData['tags'])) {
            $activity->tags()->sync($validatedData['tags']);
        }
        $activity->save();

        $activity->load('tags');

        return response()->json($activity, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        $activity->delete();
        return response(null, 204);
    }
}
