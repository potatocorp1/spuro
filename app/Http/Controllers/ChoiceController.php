<?php

namespace App\Http\Controllers;

use App\Models\Choice;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ChoiceController extends Controller
{
    private array $rules = [
        'name' => 'required|max:255',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Activity $activity)
    {
        if ($activity->user_id !== Auth::user()->id) {
            return response(null, 403);
        }

        $choices = Choice::where([
                ['activity_id', $activity->id],
            ])
            ->paginate(15);
        return response()->json($choices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Models\Activity  $activity
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Activity $activity, Request $request)
    {
        if ($activity->unit != "choice") {
            return response()->json([
                "message" => "The given activity was invalid",
                "errors" => [
                    "activity" => ["Cannot add choice on activity with no choice unit"]
                ]
            ], 422);
        }

        $validatedData = $request->validate($this->rules);

        $choice = new Choice($request->all());
        $activity->choices()->save($choice);
        return response()->json($choice, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Choice $choice)
    {
        $validatedData = $request->validate($this->rules);

        $choice->fill($request->all());
        return response()->json($choice, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Choice  $choice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Choice $choice)
    {
        if ($choice->activity->logs->count() > 0) {
            return response()->json([
                "message" => "The given choice was invalid",
                "errors" => [
                    "choice" => ["Cannot delete this choice because some logs are attached"]
                ]
            ], 422);
        }

        $choice->delete();
        return response(null, 204);
    }
}
