<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('vendor', 'node_modules');

$config = new PhpCsFixer\Config();
return $config->setFinder($finder);
