# 2. Only assign own tags

Date: 2021-05-14

## Status

Accepted

## Context

The get tags route give only tags create by the current user.

## Decision

So user can only assign own tags on activity.

## Consequences

Check on assign step that user own the tags selected to be added to activity
