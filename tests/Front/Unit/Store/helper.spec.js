import { ActionTest } from './helper.js'

describe('Refactoring testing', () => {
  it('Create helper object', done => {
    const action = jest.fn()
    const helper = new ActionTest(action, done)
    helper.exec()
    expect(action).toBeCalled()
  })

  it('Set a payload', done => {
    const action = (store, payload) => {
      expect(payload).toStrictEqual({ foo: 'bar' })
    }
    const helper = new ActionTest(action, done)
    helper
      .setPayload({ foo: 'bar' })
      .exec()
  })

  it('Set a state', done => {
    const action = ({ state }, payload) => {
      expect(state).toStrictEqual({ foo: 'bar' })
    }
    const helper = new ActionTest(action, done)
    helper
      .setState({ foo: 'bar' })
      .exec()
  })

  it('Set expected mutation', done => {
    const action = ({ commit }) => { 
      commit('MUTATION_NAME', 'bar') 
    }
    const helper = new ActionTest(action, done)
    helper
      .setMutations([{ type: 'MUTATION_NAME', payload: 'bar' }])
      .exec()
  })
})
