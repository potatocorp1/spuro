import { actions } from '@/store/index.js'
import api from '@/store/api.js'
import { ActionTest } from './helper.js'

jest.mock('@/store/api.js')

const basicLogs = [
  { id: 1, date: '2021-06-29', value: 'Foo' },
  { id: 2, date: '2021-06-30', value: 'Bar' }
]

describe('Main Store Actions - Activities', () => {
  it('get activities', done => {
    api.getActivities.mockResolvedValue({
      data: { data: [
        { id: 1, name: 'Foo', unit: 'duration' },
        { id: 2, name: 'Bar', unit: 'numeric' },
        { id: 3, name: 'Baz', unit: 'choice' }
      ] }
    })
    const state = { loadingStatus: 'NOT_YET' }
    const mutations = [
      { type: 'SET_ACTIVITIES', payload: [
        { id: 1, name: 'Foo', initLogs: false, tags: [], unit: 'duration' },
        { id: 2, name: 'Bar', initLogs: false, tags: [], unit: 'numeric' },
        { id: 3, name: 'Baz', initLogs: false, tags: [], unit: 'choice' }
      ] },
      { type: 'SET_STATUS', payload: 'OK' }
    ]

    const helper = new ActionTest(actions.getActivities, done)
    helper
      .setState(state)
      .setMutations(mutations)
      .exec()
  })

  it('add an activity', done => {
    const activity = { id: 4, name: 'Qux' }
    api.addActivity.mockResolvedValue({
      data: activity
    })
    const helper = new ActionTest(actions.addActivity, done)
    helper
      .setPayload(activity)
      .setMutations([{ type: 'ADD_ACTIVITY', payload: activity }])
      .exec()
  })

  it('update an activity', done => {
    const activity = { id: 4, name: 'Qux2' }
    api.updateActivity.mockResolvedValue({
      data: activity
    })
    const helper = new ActionTest(actions.updateActivity, done)
    helper
      .setPayload(activity)
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activity }])
      .exec()
  })

  it('delete an activity', done => {
    api.deleteActivity.mockResolvedValue({
      data: {}
    })
    const helper = new ActionTest(actions.deleteActivity, done)
    helper
      .setPayload(1)
      .setMutations([{ type: 'DELETE_ACTIVITY', payload: 1 }])
      .exec()
  })
})

describe('Main Store Actions - Tags', () => {
  it('get tags', done => {
    const tagList = [
      { id: 1, name: 'Foo' },
      { id: 2, name: 'Bar' },
      { id: 3, name: 'Baz' }
    ]
    api.getTags.mockResolvedValue({
      data: { data: tagList }
    })
    const helper = new ActionTest(actions.getTags, done)
    helper
      .setMutations([{ type: 'SET_TAGS', payload: tagList }])
      .exec()
  })

  it('add a tag', done => {
    const tag = { name: 'Foo' }
    api.addTag.mockResolvedValue({
      data: tag
    })
    const helper = new ActionTest(actions.addTag, done)
    helper
      .setPayload(tag)
      .setMutations([{ type: 'ADD_TAG', payload: tag }])
      .exec()
  })

  it('update a tag', done => {
    const tag = { id: 4, name: 'Qux number 2' }
    api.updateTag.mockResolvedValue({
      data: tag
    })
    const helper = new ActionTest(actions.updateTag, done)
    helper
      .setPayload(tag)
      .setMutations([{ type: 'UPDATE_TAG', payload: tag }])
      .exec()
  })

  it('delete a tag', done => {
    api.deleteTag.mockResolvedValue({
      data: {}
    })
    const helper = new ActionTest(actions.deleteTag, done)
    helper
      .setPayload(1)
      .setMutations([{ type: 'DELETE_TAG', payload: 1 }])
      .exec()
  })
})

describe('Main Store Actions - Choices', () => {
  it('init choices for an activity', done => {
    api.getChoices.mockResolvedValue({
      data: { data: [{ id: 1, name: 'choice1' }, { id: 2, name: 'choice2' }] }
    })
    let state = {
      activities: [{ id: 99, unit: 'choice' }],
    }
    const helper = new ActionTest(actions.initChoices, done)
    helper
      .setPayload(99)
      .setState(state)
      .setMutations([
        { type: 'UPDATE_ACTIVITY', payload: { id: 99, unit: 'choice', choices: [{ id: 1, name: 'choice1' }, { id: 2, name: 'choice2' }] } }
      ])
      .exec()
  })

  it('do not init choices for an activity of numeric or date type', done => {
    const helper = new ActionTest(actions.initChoices, done)
    helper
      .setPayload(1)
      .setState({
        activities: [{ id: 1, unit: 'date' }],
      })
      .exec()
  })

  it('do not init choices when already loaded', done => {
    const helper = new ActionTest(actions.initChoices, done)
    helper
      .setPayload(1)
      .setState({
        activities: [{ 
          id: 1,
          unit: 'choice',
          choices: [{ id: 1, name: 'choice1' }, { id: 2, name: 'choice2' }] 
        }],
      })
      .exec()
  })

  it('add a choice on an empty activity', done => {
    const choice = { id: 4, name: 'new choice' }
    const activityBefore = { id: 1, name: 'Foo', choices: [] }
    const activityAfter = { id: 1, name: 'Foo', choices: [choice] }
    api.addChoice.mockResolvedValue({
      data: choice
    })

    const helper = new ActionTest(actions.addChoice, done)
    helper
      .setPayload({ activityId: 1, name: 'new choice' })
      .setState({ activities: [activityBefore] })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })

  it('add another choice', done => {
    const choice = { id: 4, name: 'new choice' }
    const activityBefore = { id: 1, name: 'Foo', choices: [{ id: 1, name: 'choice 1' }, { id: 2, name: 'choice 2' }] }
    const activityAfter = { id: 1, name: 'Foo', choices: [{ id: 1, name: 'choice 1' }, { id: 2, name: 'choice 2' }, { id: 4, name: 'new choice' }] }
    api.addChoice.mockResolvedValue({
      data: choice
    })

    const helper = new ActionTest(actions.addChoice, done)
    helper
      .setPayload({ activityId: 1, name: 'new choice' })
      .setState({ activities: [activityBefore] })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })

  it('add another choice', done => {
    const choice = { id: 4, name: 'new choice' }
    const activityBefore = { id: 1, name: 'Foo', choices: [{ id: 1, name: 'choice 1' }, { id: 2, name: 'choice 2' }] }
    const activityAfter = { id: 1, name: 'Foo', choices: [{ id: 1, name: 'choice 1' }, { id: 2, name: 'choice 2' }, { id: 4, name: 'new choice' }] }
    api.addChoice.mockResolvedValue({
      data: choice
    })

    const helper = new ActionTest(actions.addChoice, done)
    helper
      .setPayload({ activityId: 1, name: 'new choice' })
      .setState({ activities: [activityBefore] })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })

  it('update a choice', done => {
    const choice = { id: 1, activity_id: 4, name: 'choice1 modified' }
    const activityBefore = { id: 4, name: 'Qux2', choices: [{ id: 1, activity_id: 4, name: 'choice1' }, { id: 2, activity_id: 4, name: 'choice2' }] }
    const activityAfter = { id: 4, name: 'Qux2', choices: [{ id: 1, activity_id: 4, name: 'choice1 modified' }, { id: 2, activity_id: 4, name: 'choice2' }] }
    api.updateChoice.mockResolvedValue({
      data: choice
    })

    const helper = new ActionTest(actions.updateChoice, done)
    helper
      .setPayload({ id: 1, activity_id: 4, name: 'choice1 modified' })
      .setState({ activities: [activityBefore] })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })

  it('delete a choice alone', done => {
    api.deleteChoice.mockResolvedValue({
      data: {}
    })
    const helper = new ActionTest(actions.deleteChoice, done)
    helper
      .setPayload({
        choiceId: 1,
        activityId: 1
      })
      .setState({
        activities: [{ id: 1, name: 'Foo', choices: [{ id: 1, activity_id: 1, name: 'choice1' }] }],
        logs: []
      })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: { id: 1, name: 'Foo', choices: [] } }])
      .exec()
  })

  it('delete a choice among multiple ones', done => {
    const activityAfter = { id: 1, name: 'Foo', choices: [{ id: 1, activity_id: 1, name: 'choice1' }] }
    api.deleteChoice.mockResolvedValue({
      data: {}
    })

    const helper = new ActionTest(actions.deleteChoice, done)
    helper
      .setPayload({
        choiceId: 2,
        activityId: 1
      })
      .setState({
        activities: [{ id: 1, name: 'Foo', choices: [{ id: 1, activity_id: 1, name: 'choice1' }, { id: 2, activity_id: 1, name: 'choice2' }] }],
        logs: []
      })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })

  it('do not delete a choice already logged on', done => {
    const helper = new ActionTest(actions.deleteChoice, done)
    helper
      .setPayload({
        choiceId: 3,
        activityId: 1
      })
      .setState({
        activities: [{ id: 1, name: 'Foo', choices: [{ id: 3, activity_id: 1, name: 'choice' }] }],
        logs: [{ activity_id: 1, value: 3 }]
      })
      .exec()
  })

  it('delete a choice without log but on a logged activity', done => {
    const activityAfter = { id: 1, name: 'Foo', choices: [{ id: 4, activity_id: 1, name: 'choice2' }] }
    const state = {
      activities: [{ id: 1, name: 'Foo', choices: [{ id: 3, activity_id: 1, name: 'choice1' }, { id: 4, activity_id: 1, name: 'choice2' }] }],
      logs: [{ activity_id: 1, value: 4 }]
    }
    const payload = {
      choiceId: 3,
      activityId: 1
    }

    const helper = new ActionTest(actions.deleteChoice, done)
    helper
      .setPayload(payload)
      .setState(state)
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })
})

describe('Main Store Actions - Logs', () => {
  it('init logs for an activity', done => {
    api.getLogs.mockResolvedValue({
      data: { data: basicLogs }
    })

    const helper = new ActionTest(actions.initLogs, done)
    helper
      .setPayload(1)
      .setState({ activities: [{ id: 1, initLogs: false }] })
      .setMutations([
        { type: 'SET_LOGS', payload: basicLogs },
        { type: 'INIT_LOGS_STATUS', payload: 1 }
      ])
      .exec()
  })

  it('get logs from cache when already init', done => {
    const helper = new ActionTest(actions.initLogs, done)
    helper
      .setPayload(1)
      .setState({ activities: [{ id: 1, initLogs: true }] })
      .exec()
  })

  it('add log', done => {
    const log = { id: 4, date: '2021-01-31', value: '55' }
    api.addLog.mockResolvedValue({
      data: log
    })

    const helper = new ActionTest(actions.addLog, done)
    helper
      .setPayload(log)
      .setState({ activities: [{ id: 1, initLogs: true }] })
      .setMutations([{ type: 'ADD_LOG', payload: log }])
      .exec()
  })

  it('update log', done => {
    const log = { id: 4, date: '2021-01-31', value: '55' }
    api.updateLog.mockResolvedValue({
      data: log
    })

    const helper = new ActionTest(actions.updateLog, done)
    helper
      .setPayload(log)
      .setMutations([{ type: 'UPDATE_LOG', payload: log }])
      .exec()
  })

  it('delete log', done => {
    api.deleteLog.mockResolvedValue({
      data: {}
    })
    const helper = new ActionTest(actions.deleteLog, done)
    helper
      .setPayload(1)
      .setMutations([{ type: 'DELETE_LOG', payload: 1 }])
      .exec()
  })

})
