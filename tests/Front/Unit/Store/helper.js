export class ActionTest {
  constructor (action, done) {
    this.action = action
    this.done = done
    this.payload = {}
    this.state = {}
    this.mutations = []
    this.countMutations = 0
  }

  setPayload (value) {
    this.payload = value
    return this
  }

  setState (value) {
    this.state = value
    return this
  }

  setMutations (value) {
    this.mutations = value
    return this
  }

  commit (type, payload) {
    const mutation = this.mutations[this.countMutations]
    try {
      expect(type).toEqual(mutation.type)
      expect(payload).toEqual(mutation.payload)
    } catch (error) {
      console.log(error)
      this.done(error)
    }
    this.countMutations++
    if (this.countMutations > this.mutations.length) {
      this.done()
    }
  }

  async exec () {
    const store = {
      state: this.state,
      commit: (...args) => {this.commit(...args)}
    }
    await this.action(store, this.payload)

    if (this.mutations.length === this.countMutations) {
      this.done()
    } else {
      this.done(`Expected mutations not called. Commit called ${this.countMutations} times and we expect ${this.mutations.length} mutations`)
    }
  } 
}
