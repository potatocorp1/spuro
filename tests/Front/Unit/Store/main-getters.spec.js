import { getters } from '@/store/index.js'

describe('Main Store Getters', () => {
  it('can get activity', () => {
    let state = {
      activities: [
        { id: 1, name: 'Foo' },
        { id: 2, name: 'Bar' },
        { id: 3, name: 'Baz' }
      ]
    }
    expect(getters.activity(state)(2)).toEqual({ id: 2, name: 'Bar' })
  })

  it('can get logs', () => {
    let state = {
      activities: [{ id: 1, name: 'Foo' }],
      logs: [
        { id: 1, activity_id: 1, value: '55'},
        { id: 2, activity_id: 2, value: '10'},
        { id: 3, activity_id: 1, value: '22'}
      ]
    }
    expect(getters.logs(state)(1)).toEqual([
      { id: 1, activity_id: 1, value: '55'},
      { id: 3, activity_id: 1, value: '22'}
    ])
  })

  it('can get status', () => {
    let state = {
      loadingStatus: 'OK'
    }
    expect(getters.status(state)).toEqual('OK')
  })

  it('can get tags list', () => {
    let state = {
      tags: ['Foo', 'Bar']
    }
    expect(getters.tags(state)).toEqual(['Foo', 'Bar'])
  })

  it('can get tag check status on a given activity', () => {
    let state = {
      activities: [{
        id: 1,
        tags: [10, 11, 12]
      }]
    }
    expect(getters.tagIsCheckOnActivity(state)({ tagId: 10, activityId: 1 })).toEqual(true)
  })

  it('can get tag uncheck status on a given activity', () => {
    let state = {
      activities: [{
        id: 1,
        tags: [10]
      }]
    }
    expect(getters.tagIsCheckOnActivity(state)({ tagId: 11, activityId: 1 })).toEqual(false)
  })

  it('can get choice name', () => {
    let state = {
      activities: [{ id: 1, name: 'Foo', choices: [{ id: 1, name: 'choice1' }, { id: 2, name: 'choice2' }] }]
    }
    expect(getters.choiceName(state)({ activityId: 1, choiceId: 2 })).toEqual('choice2')
  })

  it('can get last log date for an activity', () => {
    let state = {
      activities: [{ id: 1, name: 'Foo' }],
      logs: [
        { id: 1, activity_id: 1, date: '2021-06-23T00:00:00.000000Z'},
        { id: 2, activity_id: 1, date: '2021-12-02T00:00:00.000000Z'},
        { id: 3, activity_id: 1, date: '2021-12-03T00:00:00.000000Z'},
        { id: 4, activity_id: 2, date: '2021-12-31T00:00:00.000000Z'},
        { id: 5, activity_id: 1, date: '2021-04-12T00:00:00.000000Z'}
      ]
    }
    expect(getters.lastLogDate(state)(1)).toEqual('2021-12-03T00:00:00.000000Z')
  })
})
