import { actions } from '@/store/index.js'
import api from '@/store/api.js'
import { ActionTest } from './helper.js'

jest.mock('@/store/api.js')

describe('API response filtering - Activities', () => {
  it('get activities', done => {
    api.getActivities.mockResolvedValue({
      data: {
        "others": 1,
        "data": [
          {
            "id": 3,
            "name": "activityName",
            "unit": "duration",
            "user_id": 0,
            "created_at": "2019-08-24T14:15:22Z",
            "updated_at": "2019-08-24T14:15:22Z",
            "tags": [
              {
                "id": 2,
                "name": "choiceName1",
                "user_id": 0,
                "created_at": "2019-08-24T14:15:22Z",
                "updated_at": "2019-08-24T14:15:22Z"
              },
              {
                "id": 3,
                "name": "choiceName2",
                "user_id": 0,
                "created_at": "2019-08-24T14:15:22Z",
                "updated_at": "2019-08-24T14:15:22Z"
              }
            ]
          }
        ]
      }
    })

    const helper = new ActionTest(actions.getActivities, done)
    helper
      .setState({ loadingStatus: 'NOT_YET' })
      .setMutations([
        { type: 'SET_ACTIVITIES', payload: [
          { id: 3, name: 'activityName', unit: 'duration', tags: [2, 3], initLogs: false }
        ] },
        { type: 'SET_STATUS', payload: 'OK' }
      ])
      .exec()
  })

  it('add an activity', done => {
    api.addActivity.mockResolvedValue({
      "others": 1,
      "data": {
        "id": 4,
        "name": "activity name",
        "unit": "duration",
        "user_id": 0,
        "created_at": "2019-08-24T14:15:22Z",
        "updated_at": "2019-08-24T14:15:22Z",
        "tags": [{
          "id": 20,
          "name": "tag name",
          "user_id": 0,
          "created_at": "2019-08-24T14:15:22Z",
          "updated_at": "2019-08-24T14:15:22Z"
        }],
        "choices": [{
          "id": 30,
          "name": "choice name",
          "activity_id": 0,
          "created_at": "2019-08-24T14:15:22Z",
          "updated_at": "2019-08-24T14:15:22Z"
        }]
      },
      "status": 201,
      "statusText": "Created"
    })
    const activityPayload = {
      "name": "activity name",
      "unit": "duration",
      "choices": [ "choice name" ],
      "tags": [ 20 ]
    }
    const activityForStore = {
      "id": 4,
      "name": "activity name",
      "unit": "duration",
      "choices": [{ id: 30, name: "choice name" }],
      "tags": [ 20 ],
      "initLogs": true
    }
    const helper = new ActionTest(actions.addActivity, done)
    helper
      .setPayload(activityPayload)
      .setMutations([{ type: 'ADD_ACTIVITY', payload: activityForStore }])
      .exec()
  })

  it('update an activity', done => {
    api.updateActivity.mockResolvedValue({
      "others": 1,
      "data": {
        "id": 4,
        "name": "NEW activity name",
        "unit": "duration",
        "user_id": 2,
        "created_at": "2021-09-21T14:40:32.000000Z",
        "updated_at": "2021-09-21T14:43:10.000000Z",
        "tags": [{
          "id": 13,
          "name": "tag name",
          "created_at": "2021-09-03T17:42:27.000000Z",
          "updated_at": "2021-09-21T14:40:41.000000Z",
          "user_id": 2
        },{
          "id": 20,
          "name": "tag na",
          "created_at": "2021-07-05T16:47:09.000000Z",
          "updated_at": "2021-09-21T14:39:45.000000Z",
          "user_id": 2
        }]
      },
      "status": 200,
      "statusText": "OK"
    })
    const activityPayload = {
      "id": 4,
      "name": "NEW activity name",
      "tags": [ 13, 20 ]
    }
    const initialState = {
      activities: [{ 
        "id": 4, 
        "name": 'activity name',
        "unit": "duration",
        "tags": [ 13, 20 ],
        "initLogs": true
      }]
    }
    const activityForStore = {
      "id": 4,
      "name": "NEW activity name",
      "tags": [ 13, 20 ]
    }
    const helper = new ActionTest(actions.updateActivity, done)
    helper
      .setState(initialState)
      .setPayload(activityPayload)
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityForStore }])
      .exec()
  })
})

describe('API response filtering - Tags', () => {
  it('get tags', done => {
    api.getTags.mockResolvedValue({
      data: {
        "current_page": 1,
        "total":1,
        "data": [
          {
            "id": 3,
            "name": "tag name",
            "created_at": "2021-07-05T16:09:42.000000Z",
            "updated_at": "2021-07-05T17:31:10.000000Z",
            "user_id": null,
            "tag_id": null
          }
        ]
      }
    })

    const helper = new ActionTest(actions.getTags, done)
    helper
      .setMutations([{ type: 'SET_TAGS', payload: [{ "id": 3, "name": "tag name" }] }])
      .exec()
  })

  it('add a tag', done => {
    api.addTag.mockResolvedValue({
      "others": 1,
      "data": {
        "name": "new tag",
        "user_id": 2,
        "updated_at": "2021-09-21T15:19:06.000000Z",
        "created_at": "2021-09-21T15:19:06.000000Z",
        "id": 16
      }
    })
    const tagPayload = {
      "name": "new tag"
    }
    const TagForStore = {
      "id": 16,
      "name": "new tag"
    }
    const helper = new ActionTest(actions.addTag, done)
    helper
      .setPayload(tagPayload)
      .setMutations([{ type: 'ADD_TAG', payload: TagForStore }])
      .exec()
  })

  it('update a tag', done => {
    api.updateTag.mockResolvedValue({
      "others": 1,
      "data": {
        "id": 4,
        "name": "new tag name",
        "user_id": 2,
        "created_at": "2021-09-21T14:40:32.000000Z",
        "updated_at": "2021-09-21T14:43:10.000000Z",
      }
    })
    const tagPayload = {
      "id": 4,
      "name": "new tag name"
    }
    const initialState = {
      tags: [{ 
        "id": 4, 
        "name": 'old tag name',
        "user_id": 2,
        "created_at": "2021-09-21T14:40:32.000000Z",
        "updated_at": "2021-09-21T14:43:10.000000Z",
      }]
    }
    const helper = new ActionTest(actions.updateTag, done)
    helper
      .setState(initialState)
      .setPayload(tagPayload)
      .setMutations([{ type: 'UPDATE_TAG', payload: tagPayload }])
      .exec()
  })
})

describe('API response filtering - Choices', () => {
  it('init choices for an activity', done => {
    api.getChoices.mockResolvedValue({
      data: {
        "other": 1,
        "data": [{ 
          "id": 2,
          "name": "choice name",
          "activity_id": 99,
          "updated_at": "2021-09-21T15:19:06.000000Z",
          "created_at": "2021-09-21T15:19:06.000000Z"
        }]
      }
    })
    let state = {
      activities: [{ id: 99, unit: 'choice' }],
    }

    const helper = new ActionTest(actions.initChoices, done)
    helper
      .setState(state)
      .setPayload(99)
      .setMutations([{ 
        type: 'UPDATE_ACTIVITY',
        payload: { 
          id: 99, 
          unit: 'choice', 
          choices: [{ id: 2, name: 'choice name' }] } 
        }
      ])
      .exec()
  })

  it('add a choice on an activity', done => {
    api.addChoice.mockResolvedValue({
      "others": 1,
      "data": {
        "name": "new choice",
        "activity_id": 1,
        "updated_at": "2021-09-21T15:19:06.000000Z",
        "created_at": "2021-09-21T15:19:06.000000Z",
        "id": 21
      }
    })

    const activityBefore = {
      "id": 1,
      "name": 'Foo',
      "choices": []
    }
    const activityAfter = {
      "id": 1,
      "name": 'Foo',
      "choices": [{
        "id": 21,
        "name": 'new choice' 
      }]
    }

    const helper = new ActionTest(actions.addChoice, done)
    helper
      .setPayload({ activityId: 1, name: 'new choice' })
      .setState({ activities: [activityBefore] })
      .setMutations([{ type: 'UPDATE_ACTIVITY', payload: activityAfter }])
      .exec()
  })
})

describe('API response filtering - Logs', () => {
  it('init logs for an activity', done => {
    api.getLogs.mockResolvedValue({
      data: {
        "other": 1,
        "data": [{ 
          "id": 15,
          "date":	"2021-06-23T00:00:00.000000Z",
          "value": 5,
          "user_id": 2,
          "activity_id": 99,
          "updated_at": "2021-09-21T15:19:06.000000Z",
          "created_at": "2021-09-21T15:19:06.000000Z"
        }]
      }
    })
    const logsForStore = [{
      "id": 15,
      "date":	"2021-06-23T00:00:00.000000Z",
      "value": 5,
      "activity_id": 99
    }]

    const helper = new ActionTest(actions.initLogs, done)
    helper
      .setPayload(1)
      .setState({ activities: [{ id: 1, initLogs: false }] })
      .setMutations([
        { type: 'SET_LOGS', payload: logsForStore },
        { type: 'INIT_LOGS_STATUS', payload: 1 }
      ])
      .exec()
  })
})
