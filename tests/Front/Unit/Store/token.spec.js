import token from '@/store/token.js'
import api from '@/store/api.js'
import { ActionTest } from './helper.js'

const { mutations, actions } = token
jest.mock('@/store/api.js')

describe('Token Store mutation', () => {
  it('can set Token', () => {
    const tokens = ['Foo']
    let state = {
      tokens: []
    }
    mutations.setTokens(state, tokens)
    expect(state.tokens).toBe(tokens)
  })

  it('revoke token', () => {
    let state = {
      tokens: [
        {
          id: 'abc',
          revoked: false
        }
      ]
    }

    mutations.revokeToken(state, 'abc')
    expect(state.tokens[0].revoked).toBe(true)
  })

  it('can set scopes', () => {
    const scopes = ['Foo']
    let state = {
      scopes: []
    }
    mutations.setScopes(state, scopes)
    expect(state.scopes).toBe(scopes)
  })

  it('can add token', () => {
    const token = {id:'abc'}
    let state = {
      tokens: []
    }
    mutations.addToken(state, token)
    expect(state.tokens).toEqual([token])
  })
})

describe('Token Store action', () => {
  it('get all token', done => {
    api.getTokens.mockResolvedValue({
      data: [
        {
          id: 'azer123'
        }
      ]
    })

    const helper = new ActionTest(actions.getTokens, done)
    helper
      .setMutations([
      { type: 'setTokens', payload: [{id:'azer123'}]}
    ])
      .exec()
  })

  it('revoke token', done => {
    api.revokeToken.mockResolvedValue({
      data: []
    });

    const helper = new ActionTest(actions.revokeToken, done)
    helper
      .setPayload('abc')
      .setMutations([
      { type: 'revokeToken', payload: 'abc' }
    ])
      .exec()
  })

  it('get all scopes', done => {
    api.getScopes.mockResolvedValue({
      data: [
        {
          name: 'read:logs'
        }
      ]
    })

    const helper = new ActionTest(actions.getScopes, done)
    helper
      .setMutations([
        { type: 'setScopes', payload: [{name:'read:logs'}]}
      ])
      .exec()
  })

  it('create a token', done => {
    api.addToken.mockResolvedValue({
      data: {
        accessToken: 'super-long-token',
        token: {
          id:'abc'
        }
      }
    })

    const helper = new ActionTest(actions.createToken, done)
    helper
      .setPayload({name: 'Private', scopeChecked: [1]})
      .setMutations([
        { type: 'addToken', payload: {id:'abc'} }
      ])
      .exec()
  })
})
