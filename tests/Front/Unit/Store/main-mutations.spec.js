import { mutations } from '@/store/index.js'

describe('Main Store Mutations', () => {
  it('can set activities', () => {
    let state = {
      activities: []
    }
    mutations.SET_ACTIVITIES(state, ['Foo'])
    expect(state.activities).toEqual(['Foo'])
  })

  it('can add activity', () => {
    const activity = 'Foo'
    let state = {
      activities: ['Bar']
    }
    mutations.ADD_ACTIVITY(state, activity)
    expect(state.activities).toEqual(['Bar', 'Foo'])
  })

  it('can update activity', () => {
    let state = {
      activities: [
        { id: 1, name: 'Foo' },
        { id: 2, name: 'Bar' }
      ]
    }
    const result = [
      { id: 1, name: 'Foo Foo', tags: [] },
      { id: 2, name: 'Bar' }
    ]
    mutations.UPDATE_ACTIVITY(state, { id: 1, name: 'Foo Foo' })
    expect(state.activities).toEqual(result)
  })

  it('can update activity tag', () => {
    let state = {
      activities: [
        { id: 1, name: 'Foo', tags: [] }
      ]
    }
    const result = [
      { id: 1, name: 'Foo', tags: [5] }
    ]
    mutations.UPDATE_ACTIVITY(state, { id: 1, name: 'Foo', tags: [5] })
    expect(state.activities).toEqual(result)
  })

  it('can update activity choices', () => {
    let state = {
      activities: [
        { id: 1, name: 'Foo', tags: [], choices: [{ id: 2, name: 'choice name'}] }
      ]
    }
    const result = [
      { id: 1, name: 'Foo', tags: [], choices: [{ id: 2, name: 'new choice name'}] }
    ]
    mutations.UPDATE_ACTIVITY(state, { id: 1, name: 'Foo', choices: [{ id: 2, name: 'new choice name'}] })
    expect(state.activities).toEqual(result)
  })

  it('can delete activity', () => {
    let state = {
      activities: [
        { id: 1, name: 'Foo' },
        { id: 2, name: 'Bar' }
      ]
    }
    mutations.DELETE_ACTIVITY(state, 1)
    expect(state.activities).toEqual([{ id: 2, name: 'Bar' }])
  })

  it('can set loading status', () => {
    let state = {
      loadingStatus: 'NOT_YET'
    }
    mutations.SET_STATUS(state, 'OK')
    expect(state.loadingStatus).toEqual('OK')
  })

  it('can set logs for an activity', () => {
    let state = {
      logs: []
    }
    mutations.SET_LOGS(state, ['Foo'])
    expect(state.logs).toEqual(['Foo'])
  })

  it('can set logs for two activities', () => {
    let state = {
      logs: []
    }
    mutations.SET_LOGS(state, ['Log activity 1'])
    mutations.SET_LOGS(state, ['Log activity 2'])
    expect(state.logs).toEqual(['Log activity 1', 'Log activity 2'])
  })

  it('can set logs init status', () => {
    const activityId = 1
    let state = {
      activities: [{ id: activityId, initLogs: false }]
    }
    mutations.INIT_LOGS_STATUS(state, activityId)
    expect(state.activities).toEqual([{ id: activityId, initLogs: true }])
  })

  it('can add log', () => {
    const log = 'Foo'
    let state = {
      logs: ['Bar']
    }
    mutations.ADD_LOG(state, log)
    expect(state.logs).toEqual(['Bar', 'Foo'])
  })

  it('can update log date', () => {
    let state = {
      logs: [
        { id: 1, date: '2021-06-29', value: 'Foo' },
        { id: 2, date: '2021-06-30', value: 'Bar' }
      ]
    }
    const result = [
      { id: 1, date: '2021-12-01', value: 'Foo' },
      { id: 2, date: '2021-06-30', value: 'Bar' }
    ]
    mutations.UPDATE_LOG(state, { id: 1, date: '2021-12-01' })
    expect(state.logs).toEqual(result)
  })

  it('can update log value', () => {
    let state = {
      logs: [
        { id: 1, date: '2021-06-29', value: 'Foo' },
        { id: 2, date: '2021-06-30', value: 'Bar' }
      ]
    }
    const result = [
      { id: 1, date: '2021-06-29', value: 'Foo Foo' },
      { id: 2, date: '2021-06-30', value: 'Bar' }
    ]
    mutations.UPDATE_LOG(state, { id: 1, value: 'Foo Foo' })
    expect(state.logs).toEqual(result)
  })

  it('can update log date and value', () => {
    let state = {
      logs: [
        { id: 1, date: '2021-06-29', value: 'Foo' },
        { id: 2, date: '2021-06-30', value: 'Bar' }
      ]
    }
    const result = [
      { id: 1, date: '2021-12-01', value: 'Foo Foo' },
      { id: 2, date: '2021-06-30', value: 'Bar' }
    ]
    mutations.UPDATE_LOG(state, { id: 1, value: 'Foo Foo', date: '2021-12-01' })
    expect(state.logs).toEqual(result)
  })

  it('can delete log', () => {
    let state = {
      logs: [
        { id: 1, date: '2021-06-29', value: 'Foo' },
        { id: 2, date: '2021-06-30', value: 'Bar' }
      ]
    }
    mutations.DELETE_LOG(state, 1)
    expect(state.logs).toEqual([{ id: 2, date: '2021-06-30', value: 'Bar' }])
  })

  it('can set tags', () => {
    let state = {
      tags: []
    }
    mutations.SET_TAGS(state, ['Foo'])
    expect(state.tags).toEqual(['Foo'])
  })

  it('can add a tag', () => {
    const tag = 'Foo'
    let state = {
      tags: ['Bar']
    }
    mutations.ADD_TAG(state, tag)
    expect(state.tags).toEqual(['Bar', 'Foo'])
  })

  it('can update a tag', () => {
    let state = {
      tags: [
        { id: 1, name: 'Foo' },
        { id: 2, name: 'Bar' }
      ]
    }
    const result = [
      { id: 1, name: 'Foo Foo' },
      { id: 2, name: 'Bar' }
    ]
    mutations.UPDATE_TAG(state, { id: 1, name: 'Foo Foo' })
    expect(state.tags).toEqual(result)
  })

  it('can delete a tag', () => {
    let state = {
      tags: [
        { id: 1, name: 'Foo' },
        { id: 2, name: 'Bar' }
      ]
    }
    mutations.DELETE_TAG(state, 1)
    expect(state.tags).toEqual([{ id: 2, name: 'Bar' }])
  })
})