import user from '@/store/user.js'
import api from '@/store/api.js'
import { ActionTest } from './helper.js'

const { mutations, getters, actions } = user
jest.mock('@/store/api.js')

describe('User Store mutation', () => {
  it('can set username', () => {
    const username = 'Foo'
    let state = {
      username: ''
    }
    mutations.setUsername(state, username)
    expect(state.username).toMatch(username)
  })
})

describe('User Store getter', () => {
  it('check no authentificated user', () => {
    const state = {
      username: ''
    }

    expect(getters.isAuthentificated(state)).toBe(false)
  })

  it('check authentificated user', () => {
    const state = {
      username: 'foo'
    }

    expect(getters.isAuthentificated(state)).toBe(true)
  })
})


describe('User Store action', () => {
  it('get current user', done => {

    api.getUser.mockResolvedValue({
      data: {
        name: 'foo'
      }
    });

    const helper = new ActionTest(actions.getUser, done)
    helper
      .setMutations([
        { type: 'setUsername', payload: 'foo' }
      ])
      .exec()
  })
})
