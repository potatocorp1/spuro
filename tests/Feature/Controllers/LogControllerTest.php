<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;
use App\Models\Choice;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class LogControllerTest extends TestCase
{
    use RefreshDatabase;
    
    private User $user;
    private Activity $activity;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->activity = activity::factory()->create(['user_id' => $this->user->id]);
        Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
        ]);
    }

    /**
     * @return void
     */
    public function testGetLogsWithWrongToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetLogsAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetLogs(): void
    {
        Passport::actingAs($this->user, ['read:logs']);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetLogsPagination(): void
    {
        Passport::actingAs($this->user, ['read:logs']);
        Log::factory()->count(15)->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
        ]);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $this->assertCount(15, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetLogsOnlyForMyUser(): void
    {
        Passport::actingAs($this->user, ['read:logs']);

        $otherUser = User::factory()->create();
        Log::factory()->create([
            'user_id' => $otherUser->id,
            'activity_id' => $this->activity->id,
        ]);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetLogsOnlyForThisActivity(): void
    {
        Passport::actingAs($this->user, ['read:logs']);

        $otherActivity = activity::factory()->create(['user_id' => $this->user->id]);
        Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $otherActivity->id,
        ]);

        $response = $this->get('/api/activities/'.$this->activity->id.'/logs');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testCreateLogWrongScope(): void
    {
        Passport::actingAs($this->user, ['read:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testCreateLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
            'value' => 45
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'date' => '2021-01-01T13:00:00.000000Z',
                'value' => 45,
                'user_id' => $this->user->id,
                'activity_id' => $this->activity->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCreateRequireDateLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'value' => 45
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['date' => 'The date field is required']);
    }

    /**
     * @return void
     */
    public function testCreateRequireValueLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The value field is required']);
    }

    /**
     * @return void
     */
    public function testCreateDateFormatLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => 'tomorrow',
            'value' => 45,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['date' => 'The date is not a valid date.']);
    }

    /**
     * @return void
     */
    public function testCreateValueLogMustBeInteger(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
            'value' => 45.23,
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The value must be an integer.']);
    }

    /**
     * @return void
     */
    public function testUpdateLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $log = Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
            'date' => '2021-05-05 13:00:00',
            'value' => 40
        ]);

        $response = $this->putJson('/api/logs/'.$log->id, [
            'date' => '2021-01-01 13:00:00',
            'value' => 30
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $log->id,
                'date' => '2021-01-01T13:00:00.000000Z',
                'value' => 30,
                'user_id' => $this->user->id,
                'activity_id' => $this->activity->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testDeleteLog(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $response = $this->delete('/api/logs/1');

        $response->assertStatus(204);
        $this->assertDatabaseMissing('logs', [
            'id' => 1,
        ]);
    }

    /**
     * @return void
     */
    public function testCreateLogWithWrongChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
            'value' => 45
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The selected value is invalid']);
    }

    /**
     * @return void
     */
    public function testCreateLogWithOtherChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $choice = Choice::factory()->create([
            'activity_id' => 1
        ]);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
            'value' => $choice->id
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The selected value is invalid']);
    }
    /**
     * @return void
     */
    public function testCreateLogWithChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/logs', [
            'date' => '2021-01-01 13:00:00',
            'value' => $choices[1]->id
        ]);

        $response->assertStatus(201);
    }

    /**
     * @return void
     */
    public function testUpdateLogWithWrongChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $log = Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
            'date' => '2021-05-05 13:00:00',
            'value' => $choices[0]->id
        ]);

        $response = $this->putJson('/api/logs/'.$log->id, [
            'date' => '2021-01-01 13:00:00',
            'value' => 45
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The selected value is invalid']);
    }

    /**
     * @return void
     */
    public function testUpdateLogWithOtherChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $log = Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
            'date' => '2021-05-05 13:00:00',
            'value' => $choices[0]->id
        ]);

        $choice = Choice::factory()->create([
            'activity_id' => 1
        ]);

        $response = $this->putJson('/api/logs/'.$log->id, [
            'date' => '2021-01-01 13:00:00',
            'value' => $choice->id
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['value' => 'The selected value is invalid']);
    }
    /**
     * @return void
     */
    public function testUpdateLogWithChoice(): void
    {
        Passport::actingAs($this->user, ['write:logs']);

        $this->activity = activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'choice'
        ]);

        $choices = Choice::factory()->count(3)->create([
            'activity_id' => $this->activity->id
        ]);

        $log = Log::factory()->create([
            'user_id' => $this->user->id,
            'activity_id' => $this->activity->id,
            'date' => '2021-05-05 13:00:00',
            'value' => $choices[0]->id
        ]);

        $response = $this->putJson('/api/logs/'.$log->id, [
            'date' => '2021-01-01 13:00:00',
            'value' => $choices[1]->id
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $log->id,
                'date' => '2021-01-01T13:00:00.000000Z',
                'value' => $choices[1]->id,
                'user_id' => $this->user->id,
                'activity_id' => $this->activity->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }
}
