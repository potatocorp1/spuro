<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\Tag;
use App\Models\User;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class SharingControllerTest extends TestCase
{
    use RefreshDatabase;
    
    private User $user;
    private Tag $tag;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->tag = Tag::factory()->create(['user_id' => $this->user->id]);
    }

    /**
     * @return void
     */
    public function testGetSharingsWithWrongToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/sharings');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetSharingsAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $user = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $user->id]);
        $tag->sharing()->attach($this->user);
        $response = $this->get('/api/sharings');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetSharings(): void
    {
        Passport::actingAs($this->user, ['read:sharings']);

        $user = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $user->id]);
        $tag->sharing()->attach($this->user);
        $response = $this->get('/api/sharings');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'tag_id' => 2,
                        'guest_id' => 2,
                        'guest_name' => $user->name,
                    ]
                ]
            ]);
    }

    /**
     * @return void
     */
    public function testGetSharingsPagination(): void
    {
        Passport::actingAs($this->user, ['read:sharings']);

        $user = User::factory()->create();
        $tags = Tag::factory()->count(16)->create(['user_id' => $user->id]);
        $this->user->sharing()->attach($tags);
        $response = $this->get('/api/sharings');

        $this->assertCount(15, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetSharingsOnlyForMyUser(): void
    {
        Passport::actingAs($this->user, ['read:sharings']);

        $user = User::factory()->create();
        $tags = Tag::factory()->count(3)->create(['user_id' => $user->id]);
        $this->user->sharing()->attach($tags);
        $user->sharing()->attach($this->tag);
        $response = $this->get('/api/sharings');

        $this->assertCount(3, $response['data']);
        $response->assertStatus(200);
        $this->assertDatabaseCount('sharing', 4);
    }

    /**
     * @return void
     */
    public function testGetSharingsWrongScope(): void
    {
        Passport::actingAs($this->user, ['read:tags']);

        $response = $this->get('/api/sharings');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testCreateSharing(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $user = User::factory()->create();

        $response = $this->putJson('/api/tags/1/sharing', [
            'email' => $user->email
        ]);

        $response->assertStatus(204);
        $this->assertDatabaseHas('sharing', [
            'user_id' => $user->id,
            'tag_id' => $this->tag->id
        ]);
    }

    /**
     * @return void
     */
    public function testCreateSharingWrongScope(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $user = User::factory()->create();

        $response = $this->putJson('/api/tags/1/sharing', [
            'email' => $user->email
        ]);

        $response->assertStatus(403);
    }
    /**
     * @return void
     */
    public function testEmailRequireToCreateSharing(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $response = $this->putJson('/api/tags/1/sharing', [
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['email' => 'The email field is required']);
    }

    /**
     * @return void
     */
    public function testEmailFormatCorrectToCreateSharing(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $response = $this->putJson('/api/tags/1/sharing', [
            'email' => 'foo'
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['email' => 'The email must be a valid email address.']);
    }

    /**
     * @return void
     */
    public function testCreateShouldNotAcceptUnknowEmail(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $response = $this->putJson('/api/tags/1/sharing', [
            'email' => 'foo@local.lan'
        ]);

        $response->assertStatus(422)
            ->assertJsonValidationErrors(['email' => 'The selected email is invalid']);
    }

    /**
     * @return void
     */
    public function testDeleteSharing(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $guest = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $this->user->id]);
        $tag->sharing()->attach($guest);

        $response = $this->deleteJson('/api/tags/' . $tag->id . '/sharing/' . $guest->id);

        $response->assertStatus(204);
        $this->assertDatabaseMissing('sharing', [
            'user_id' => $guest->id,
            'tag_id' => $tag->id,
        ]);
    }

    /**
     * @return void
     */
    public function testDeleteSharingByGuest(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $owner = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $owner->id]);
        $tag->sharing()->attach($this->user);

        $response = $this->deleteJson('/api/tags/' . $tag->id . '/sharing/' . $this->user->id);

        $response->assertStatus(204);
        $this->assertDatabaseMissing('sharing', [
            'user_id' => $this->user->id,
            'tag_id' => $tag->id,
        ]);
    }

    /**
     * @return void
     */
    public function testAnotherCannotDeleteSharing(): void
    {
        Passport::actingAs($this->user, ['write:sharings']);

        $owner = User::factory()->create();
        $guest = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $owner->id]);
        $tag->sharing()->attach($guest);

        $response = $this->deleteJson('/api/tags/' . $tag->id . '/sharing/' . $guest->id);

        $response->assertStatus(403);
        $this->assertDatabaseHas('sharing', [
            'user_id' => $guest->id,
            'tag_id' => $tag->id,
        ]);
    }
}
