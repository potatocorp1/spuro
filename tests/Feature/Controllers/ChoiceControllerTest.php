<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Choice;
use App\Models\Log;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class ChoiceControllerTest extends TestCase
{
    use RefreshDatabase;

    private User $user;
    private Activity $activity;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->activity = activity::factory()->create([
            'unit' => 'choice',
            'user_id' => $this->user->id
        ]);
        Choice::factory()->create([
            'activity_id' => $this->activity->id,
        ]);
    }

    /**
     * @return void
     */
    public function testGetChoicesWithWrongToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/activities/'.$this->activity->id.'/choices');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetChoicesAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $response = $this->get('/api/activities/'.$this->activity->id.'/choices');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetChoicesNoOwnedActivity(): void
    {
        Passport::actingAs($this->user, ['all']);

        $user = User::factory()->create();
        $activity = activity::factory()->create(['user_id' => $user->id]);

        $response = $this->get('/api/activities/'.$activity->id.'/choices');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetChoices(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $response = $this->get('/api/activities/'.$this->activity->id.'/choices');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetChoicesPagination(): void
    {
        Passport::actingAs($this->user, ['read:activities']);
        Choice::factory()->count(15)->create([
            'activity_id' => $this->activity->id,
        ]);

        $response = $this->get('/api/activities/'.$this->activity->id.'/choices');

        $this->assertCount(15, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetChoicesOnlyForThisActivity(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $otherActivity = activity::factory()->create(['user_id' => $this->user->id]);
        Choice::factory()->create([
            'activity_id' => $otherActivity->id,
        ]);

        $response = $this->get('/api/activities/'.$this->activity->id.'/choices');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testCreateChoiceWrongScope(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/choices');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testCreateChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/choices', [
            'name' => 'foo'
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'name' => 'foo',
                'activity_id' => $this->activity->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);

        $this->assertDatabaseHas('choices', [
            'name' => 'foo',
            'activity_id' => $this->activity->id,
        ]);
    }

    /**
     * @return void
     */
    public function testCreateChoiceRequireName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/choices', []);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name field is required']);
    }

    /**
     * @return void
     */
    public function testCreateChoiceShortName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities/'.$this->activity->id.'/choices', [
            'name' => Str::random(256),
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name may not be greater than 255 characters.']);
    }

    /**
     * @return void
     */
    public function testCannotCreateChoiceForActivityWithOtherUnit(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $activity = Activity::factory()->create([
            'unit' => 'duration',
            'user_id' => $this->user->id
        ]);

        $response = $this->postJson('/api/activities/'.$activity->id.'/choices', [
            'name' => 'foo',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['activity' => 'Cannot add choice on activity with no choice unit']);
    }

    /**
     * @return void
     */
    public function testUpdateChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $choice = Choice::factory()->create([
            'activity_id' => $this->activity->id,
            'name' => 'foo'
        ]);

        $response = $this->putJson('/api/choices/'.$choice->id, [
            'name' => 'bar',
            'activity_id' => 1337
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $choice->id,
                'name' => 'bar',
                'activity_id' => $this->activity->id
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testUpdateChoiceRequireName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $choice = Choice::factory()->create([
            'activity_id' => $this->activity->id,
            'name' => 'foo'
        ]);

        $response = $this->putJson('/api/choices/'.$choice->id, []);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name field is required']);
    }

    /**
     * @return void
     */
    public function testUpdateChoiceShortName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $choice = Choice::factory()->create([
            'activity_id' => $this->activity->id,
            'name' => 'foo'
        ]);

        $response = $this->putJson('/api/choices/'.$choice->id, [
            'name' => Str::random(256),
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name may not be greater than 255 characters.']);
    }

    /**
     * @return void
     */
    public function testDeleteChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->delete('/api/choices/1');

        $response->assertStatus(204);
        $this->assertDatabaseMissing('choices', [
            'id' => 1,
        ]);
    }

    /**
     * @return void
     */
    public function testCannotDeleteChoiceWithLog(): void
    {
        Passport::actingAs($this->user, ['write:activities']);
        $log = Log::factory()->create([
            'activity_id' => $this->activity->id,
            'user_id' => $this->user->id,
            'value' => 1
        ]);

        $response = $this->delete('/api/choices/1');

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['choice' => 'Cannot delete this choice because some logs are attached']);
        $this->assertDatabaseHas('choices', [
            'id' => 1,
        ]);
    }
}
