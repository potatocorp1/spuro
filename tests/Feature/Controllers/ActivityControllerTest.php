<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Tag;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class ControllersActivityControllerTest extends TestCase
{
    use RefreshDatabase;
    
    private User $user;
    private Activity $activity;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->activity = Activity::factory()->create(['user_id' => $this->user->id]);
    }

    /**
     * @return void
     */
    public function testGetActivitiesWithWrongToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/activities');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetActivitiesAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $response = $this->get('/api/activities');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetActivities(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $response = $this->get('/api/activities');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetActivitiesPagination(): void
    {
        Passport::actingAs($this->user, ['read:activities']);
        Activity::factory()->count(15)->create(['user_id' => $this->user->id]);

        $response = $this->get('/api/activities');

        $this->assertCount(15, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetActivitiesOnlyForMyUser(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $otherUser = User::factory()->create();
        Activity::factory()->create(['user_id' => $otherUser->id]);

        $response = $this->get('/api/activities');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetActivitiesWithTags(): void
    {
        Passport::actingAs($this->user, ['read:activities']);
        $tag = Tag::factory()->create(['user_id' => $this->user->id]);
        $this->activity->tags()->attach($tag->id);

        $response = $this->get('/api/activities');

        $this->assertCount(1, $response['data']);
        $this->assertArrayHasKey('tags', $response['data'][0]);
        $this->assertEquals([
            [
                'id' => $tag->id,
                'name' => $tag->name,
                'user_id' => $tag->user_id,
            ]
        ], $response['data'][0]['tags']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testCreateActivityWrongScope(): void
    {
        Passport::actingAs($this->user, ['read:activities']);

        $response = $this->postJson('/api/activities');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testCreateActivity(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'duration',
        ]);
    
        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'name' => 'cook',
                'unit' => 'duration',
                'user_id' => $this->user->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCreateActivityRequireName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'unit' => 'duration',
        ]);
        
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name field is required']);
    }

    /**
     * @return void
     */
    public function testCreateActivityRequireUnit(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
        ]);
        
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['unit' => 'The unit field is required']);
    }

    /**
     * @return void
     */
    public function testCreateActivityTooLongName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => Str::random(256),
            'unit' => 'duration',
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name may not be greater than 255 characters.']);
    }

    /**
     * @return void
     */
    public function testCreateActivityUnknowUnit(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'foo',
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['unit' => 'The selected unit is invalid.']);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $tags = Tag::factory()->count(3)->create();

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'duration',
            'tags' => [
                $tags[0]->id,
                $tags[2]->id,
            ]
        ]);
    
        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'name' => 'cook',
                'unit' => 'duration',
                'user_id' => $this->user->id,
                'tags' => [
                    $tags[0]->toArray(),
                    $tags[2]->toArray(),
                ]
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCreateActivityTagRequireName(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'duration',
            'tags' => ['']
        ]);
        
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tags.0' => 'The tags.0 field is required.']);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithWrongTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'duration',
            'tags' => [1]
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tags.0' => 'The selected tags.0 is invalid']);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithDontOwnTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $user = User::factory()->create();
        $tag = Tag::factory()->create([
            'user_id' => $user->id
        ]);
        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'duration',
            'tags' => [$tag->id]
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tags.0' => 'The selected tags.0 is invalid']);
    }



    /**
     * @return void
     */
    public function testUpdateActivity(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        Activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'numeric',
        ]);
        
        $user = User::factory()->create();
        $response = $this->putJson('/api/activities/2', [
            'name' => 'cook',
            'unit' => 'duration',
            'user_id' => $user->id,
        ]);
    
        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => 2,
                'name' => 'cook',
                'unit' => 'numeric',
                'user_id' => $this->user->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testUpdateActivityWithTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        Activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'numeric',
        ]);

        $tags = Tag::factory()->count(3)->create();

        $user = User::factory()->create();
        $response = $this->putJson('/api/activities/2', [
            'name' => 'cook',
            'tags' => [
                $tags[0]->id,
                $tags[2]->id,
            ]
        ]);
    
        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => 2,
                'name' => 'cook',
                'unit' => 'numeric',
                'user_id' => $this->user->id,
                'tags' => [
                    $tags[0]->toArray(),
                    $tags[2]->toArray(),
                ]
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testUpdateActivityWithWrongTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        Activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'numeric',
        ]);

        $user = User::factory()->create();
        $response = $this->putJson('/api/activities/2', [
            'name' => 'cook',
            'tags' => [1]
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tags.0' => 'The selected tags.0 is invalid']);
    }

    /**
     * @return void
     */
    public function testUpdateActivityWithDontOwnTag(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        Activity::factory()->create([
            'user_id' => $this->user->id,
            'unit' => 'numeric',
        ]);

        $user = User::factory()->create();
        $tag = Tag::factory()->create([
            'user_id' => $user->id
        ]);
        $response = $this->putJson('/api/activities/2', [
            'name' => 'cook',
            'tags' => [$tag->id]
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tags.0' => 'The selected tags.0 is invalid']);
    }

    /**
     * @return void
     */
    public function testDeleteActivity(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $user = User::factory()->create();

        $response = $this->delete('/api/activities/1');

        $response->assertStatus(204);
        $this->assertDatabaseMissing('activities', [
            'id' => 1,
        ]);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'choice',
            'choices' => [
                'starter',
                'main course',
                'dessert',
            ],
        ]);
    
        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'name' => 'cook',
                'unit' => 'choice',
                'choices' => [
                    [
                        'id' => 1,
                        'name' => 'starter',
                    ],
                    [
                        'id' => 2,
                        'name' => 'main course',
                    ],
                    [
                        'id' => 3,
                        'name' => 'dessert',
                    ]
                ],
                'user_id' => $this->user->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithNoChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'choice',
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['choices' => 'The choices field is required']);
    }

    /**
     * @return void
     */
    public function testCreateActivityWithoutChoice(): void
    {
        Passport::actingAs($this->user, ['write:activities']);

        $response = $this->postJson('/api/activities', [
            'name' => 'cook',
            'unit' => 'choice',
            'choices' => [''],
        ]);
    
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['choices.0' => 'The choices.0 field is required']);
    }
}
