<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;
    
    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    /**
     * @return void
     */
    public function testGetCurrentUserWithNoToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/user');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetCurrentUserAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $response = $this->get('/api/user');

        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetCurrentUser(): void
    {
        Passport::actingAs($this->user, ['read:users']);

        $response = $this->get('/api/user');

        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => $this->user->id,
                'name' => $this->user->name,
                'email' => $this->user->email,
            ]);
    }
}
