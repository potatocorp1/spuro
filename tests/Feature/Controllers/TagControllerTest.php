<?php

namespace Tests\Feature\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\Tag;
use App\Models\User;
use Laravel\Passport\Passport;
use Illuminate\Support\Str;

class TagControllerTest extends TestCase
{
    use RefreshDatabase;
    
    private User $user;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        Tag::factory()->create(['user_id' => $this->user->id]);
    }

    /**
     * @return void
     */
    public function testGetTagsWithWrongToken(): void
    {
        Passport::actingAs($this->user);

        $response = $this->get('/api/tags');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testGetTagsAllScope(): void
    {
        Passport::actingAs($this->user, ['all']);

        $response = $this->get('/api/tags');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetTags(): void
    {
        Passport::actingAs($this->user, ['read:tags']);

        $response = $this->get('/api/tags');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetTagsPagination(): void
    {
        Passport::actingAs($this->user, ['read:tags']);
        Tag::factory()->count(15)->create(['user_id' => $this->user->id]);

        $response = $this->get('/api/tags');

        $this->assertCount(15, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetTagsOnlyForMyUser(): void
    {
        Passport::actingAs($this->user, ['read:tags']);

        $otherUser = User::factory()->create();
        Tag::factory()->create(['user_id' => $otherUser->id]);

        $response = $this->get('/api/tags');

        $this->assertCount(1, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testGetSharedTagsForMyUser(): void
    {
        Passport::actingAs($this->user, ['read:tags']);

        $otherUser = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $otherUser->id]);
        $this->user->sharing()->attach($tag);
        $response = $this->get('/api/tags');

        $this->assertCount(2, $response['data']);
        $response->assertStatus(200);
    }

    /**
     * @return void
     */
    public function testCreateTagWrongScope(): void
    {
        Passport::actingAs($this->user, ['read:tags']);

        $response = $this->postJson('/api/tags');

        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testCreateTag(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $response = $this->postJson('/api/tags', [
            'name' => 'outside',
        ]);
    
        $response
            ->assertStatus(201)
            ->assertJson([
                'id' => 2,
                'name' => 'outside',
                'user_id' => $this->user->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCreateTagRequireName(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $response = $this->postJson('/api/tags', []);
        
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name field is required']);
    }

    /**
     * @return void
     */
    public function testCreateTagTooLongName(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $response = $this->postJson('/api/tags', [
            'name' => Str::random(256),
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name may not be greater than 255 characters.']);
    }

    /**
     * @return void
     */
    public function testCreateDuplicateTag(): void
    {
        Passport::actingAs($this->user, ['write:tags']);
        $tag = Tag::factory()->create(['user_id' => $this->user->id, 'name' => 'sport']);

        $response = $this->postJson('/api/tags', [
            'name' => 'sport',
        ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['name' => 'The name has already been taken.']);
    }

    /**
     * @return void
     */
    public function testCreateTagLimited(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        Tag::factory()->count(20)->create(['user_id' => $this->user->id]);
        $response = $this->postJson('/api/tags', [
            'name' => 'foo'
        ]);
        
        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['tag' => 'Too many tags, limit is 20']);
    }

    /**
     * @return void
     */
    public function testUpdateTag(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $response = $this->putJson('/api/tags/1', [
            'name' => 'outside',
        ]);
    
        $response
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'name' => 'outside',
                'user_id' => $this->user->id,
            ])
            ->assertJsonStructure([
                'created_at',
                'updated_at',
            ]);
    }

    /**
     * @return void
     */
    public function testCannotUpdateOtherUserTag(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $user = User::factory()->create();
        $tag = Tag::factory()->create(['user_id' => $user->id ]);

        $response = $this->putJson('/api/tags/'.$tag->id, [
            'name' => 'outside',
        ]);
    
        $response->assertStatus(403);
    }

    /**
     * @return void
     */
    public function testDeleteTag(): void
    {
        Passport::actingAs($this->user, ['write:tags']);

        $response = $this->delete('/api/tags/1');

        $response->assertStatus(204);
        $this->assertDatabaseMissing('activities', [
            'id' => 1,
        ]);
    }
}
