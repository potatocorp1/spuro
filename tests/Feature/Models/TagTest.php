<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Tag;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TagTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMassAssignable(): void
    {
        $tag = new Tag([
            'name' => 'Out Side',
        ]);

        $this->assertEquals('Out Side', $tag->name);
    }

    /**
     * @return void
     */
    public function testCasts(): void
    {
        $tag = new Tag();
        $tag->name = 1337;
        $tag->user_id = "17";

        $this->assertSame("1337", $tag->name);
        $this->assertSame(17, $tag->user_id);
    }

    /**
     * @return void
     */
    public function testActivities(): void
    {
        $user = User::factory()->create();
        $tag = new Tag([
            'name' => 'sport',
        ]);
        $tag->user_id = $user->id;
        $tag->save();

        Activity::factory()->create(['user_id' => $user->id]);
        
        $activity = Activity::first();

        $tag->activities()->attach($activity->id);

        $this->assertEquals($activity->name, $tag->activities[0]->name);
        $this->assertInstanceOf(BelongsToMany::class, $tag->activities());
    }

    /**
     * @return void
     */
    public function testUser(): void
    {
        User::factory()->create();

        $user = User::first();
        $tag = new Tag();
        $tag->user_id = $user->id;

        $this->assertEquals($user, $tag->user);
        $this->assertInstanceOf(BelongsTo::class, $tag->user());
    }

    /**
     * @return void
     */
    public function testSharing(): void
    {
        $user = User::factory()->create();
        $tag = Tag::factory()->create();
        $tag->sharing()->attach($user);

        $user = User::first();
        $tag = Tag::first();

        $this->assertEquals($user->name, $tag->sharing[0]->name);
        $this->assertInstanceOf(BelongsToMany::class, $tag->sharing());
    }
}
