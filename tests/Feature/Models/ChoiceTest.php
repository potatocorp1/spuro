<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;
use App\Models\Choice;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ChoiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMassAssignable(): void
    {
        $choice = new Choice([
            'name' => 'foo',
            'activity_id' => 1,
        ]);

        $this->assertEquals('foo', $choice->name);
        $this->assertEquals(null, $choice->activity_id);
    }

    /**
     * @return void
     */
    public function testCasts(): void
    {
        $choice = new Choice();
        $choice->name = 1;
        $choice->activity_id = "1";

        $this->assertSame("1", $choice->name);
        $this->assertSame(1, $choice->activity_id);
    }

    /**
     * @return void
     */
    public function testActivity(): void
    {
        $user = User::factory()->create();
        Activity::factory()->create(['user_id' => $user->id]);

        $activity = Activity::first();
        $choice = new Choice();
        $choice->activity_id = $activity->id;

        $this->assertEquals($activity, $choice->activity);
        $this->assertInstanceOf(BelongsTo::class, $choice->activity());
    }
}
