<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;
use App\Models\Tag;
use App\Models\Choice;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ActivityTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMassAssignable(): void
    {
        $user = User::factory()->create();
        $activity = new Activity([
            'name' => 'Run',
            'unit' => 'duration',
        ]);
        $activity->user_id = $user->id;

        $this->assertEquals('Run', $activity->name);
        $this->assertEquals('duration', $activity->unit);
    }

    /**
     * @return void
     */
    public function testCasts(): void
    {
        $activity = new Activity();
        $activity->name = 1337;
        $activity->unit = false;

        $this->assertSame('1337', $activity->name);
        $this->assertSame('', $activity->unit);
    }

    /**
     * @return void
     */
    public function testUser(): void
    {
        User::factory()->create();

        $user = User::first();
        $activity = new Activity();
        $activity->user_id = $user->id;

        $this->assertEquals($user, $activity->user);
        $this->assertInstanceOf(BelongsTo::class, $activity->user());
    }

    /**
     * @return void
     */
    public function testLogs(): void
    {
        $user = User::factory()->create();
        $activity = Activity::factory()->create(['user_id' => $user->id]);
        Log::factory()->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
        ]);

        $user = User::first();
        $activity = Activity::first();
        $log = Log::first();

        $this->assertEquals($log, $activity->logs[0]);
        $this->assertInstanceOf(HasMany::class, $activity->logs());
    }

    /**
     * @return void
     */
    public function testTags(): void
    {
        $user = User::factory()->create();
        $tag = new Tag([
            'name' => 'sport',
        ]);
        $tag->user_id = $user->id;
        $tag->save();

        $activity = Activity::factory()->create(['user_id' => $user->id]);

        $activity->tags()->attach($tag->id);

        $this->assertEquals($tag->name, $activity->tags[0]->name);
        $this->assertInstanceOf(BelongsToMany::class, $activity->tags());
    }

    /**
     * @return void
     */
    public function testChoices(): void
    {
        $user = User::factory()->create();
        $activity = Activity::factory()->create(['user_id' => $user->id]);
        Choice::factory()->create([
            'activity_id' => $activity->id,
        ]);

        $activity = Activity::first();
        $choice = Choice::first();

        $this->assertEquals($choice, $activity->choices[0]);
        $this->assertInstanceOf(HasMany::class, $activity->choices());
    }
}
