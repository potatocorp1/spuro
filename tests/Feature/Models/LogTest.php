<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;
use DateTime;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LogTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMassAssignable(): void
    {
        $now = new DateTime();

        $log = new Log([
            'date' => $now,
            'value' => 1,
            'user_id' => 1,
            'activity_id' => 1,
        ]);

        $this->assertEquals($now->format('Y-m-d'), $log->date->format('Y-m-d'));
        $this->assertEquals(1, $log->value);
        $this->assertEquals(null, $log->user_id);
        $this->assertEquals(null, $log->activity_id);
    }

    /**
     * @return void
     */
    public function testCasts(): void
    {
        $log = new Log();
        $log->date = "2021-01-01";
        $log->value = "1";
        $log->user_id = "1";
        $log->activity_id = "1";

        $this->assertEquals(new DateTime("2021-01-01"), $log->date);
        $this->assertSame(1, $log->value);
        $this->assertSame(1, $log->user_id);
        $this->assertSame(1, $log->activity_id);
    }

    /**
     * @return void
     */
    public function testUser(): void
    {
        User::factory()->create();

        $user = User::first();
        $log = new Log();
        $log->user_id = $user->id;

        $this->assertEquals($user, $log->user);
        $this->assertInstanceOf(BelongsTo::class, $log->user());
    }

    /**
     * @return void
     */
    public function testActivity(): void
    {
        $user = User::factory()->create();
        Activity::factory()->create(['user_id' => $user->id]);

        $activity = Activity::first();
        $log = new Log();
        $log->activity_id = $activity->id;

        $this->assertEquals($activity, $log->activity);
        $this->assertInstanceOf(BelongsTo::class, $log->activity());
    }
}
