<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use App\Models\Log;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testMassAssignable(): void
    {
        $user = User::create([
            'name' => 'John',
            'email' => 'john@local.lan',
            'password' => 'password',
        ]);

        $this->assertEquals('John', $user->name);
        $this->assertEquals('john@local.lan', $user->email);
        $this->assertEquals('password', $user->password);
    }

    /**
     * @return void
     */
    public function testHidden(): void
    {
        $user = User::factory()->create()->toArray();
        $this->assertArrayNotHasKey('password', $user);
        $this->assertArrayNotHasKey('remember_token', $user);
    }


    /**
     * @return void
     */
    public function testCasts(): void
    {
        $user = new User();
        $user->email_verified_at = '2021-03-20';
        
        $this->assertEquals('2021-03-20 00:00:00', $user->email_verified_at);
    }

    /**
     * @return void
     */
    public function testActivity(): void
    {
        $user = User::factory()->create();
        Activity::factory()->create(['user_id' => $user]);

        $user = User::first();
        $activity = Activity::first();

        $this->assertEquals($activity, $user->activities[0]);
        $this->assertInstanceOf(HasMany::class, $user->activities());
    }

    /**
     * @return void
     */
    public function testLogs(): void
    {
        $user = User::factory()->create();
        $activity = Activity::factory()->create(['user_id' => $user->id]);
        Log::factory()->create([
            'user_id' => $user->id,
            'activity_id' => $activity->id,
        ]);

        $user = User::first();
        $activity = Activity::first();
        $log = Log::first();

        $this->assertEquals($log, $user->logs[0]);
        $this->assertInstanceOf(HasMany::class, $user->logs());
    }

    /**
     * @return void
     */
    public function testSharing(): void
    {
        $user = User::factory()->create();
        $tag = Tag::factory()->create();
        $user->sharing()->attach($tag);

        $user = User::first();
        $tag = Tag::first();

        $this->assertEquals($tag->name, $user->sharing[0]->name);
        $this->assertInstanceOf(BelongsToMany::class, $user->sharing());
    }
}
