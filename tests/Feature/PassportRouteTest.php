<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Activity;
use App\Models\User;
use Laravel\Passport\Passport;

class PassportRouteTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @return void
     */
    public function testGetScope(): void
    {
        Passport::actingAs(User::factory()->create());

        $response = $this->get('/oauth/scopes');

        $response->assertStatus(200);
        $response->assertJsonCount(10);
    }
}
